set -x
#sudo apt-get update -qq
sudo sed -i 's/\# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen
sudo /usr/sbin/locale-gen

#sudo apt-get install -y ptpd ccze ntpdate
HOSTNAME=$(hostname -s)
case ${HOSTNAME} in

    ptp1)
      echo 'START_DAEMON=yes' | sudo tee /etc/default/ptpd;
      echo 'PTPD_OPTS="-D -b enp0s8 -G -y 0 -f /var/log/ptpd.log"' | sudo tee -a /etc/default/ptpd;
      #sudo ntpdate -u pool.ntp.org
      ;;

    *)
      echo 'START_DAEMON=yes' | sudo tee /etc/default/ptpd;
      echo 'PTPD_OPTS="-D -b enp0s8 -g -f /var/log/ptpd.log"' | sudo tee -a /etc/default/ptpd;
      ;;
esac
