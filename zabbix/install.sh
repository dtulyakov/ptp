sudo apt-get update -qq
sudo sed -i 's/\# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen
sudo /usr/sbin/locale-gen
sudo apt install software-properties-common python-apt python wget -y
sudo wget "https://gitlab.com/dtulyakov/docker-compose/raw/master/docker-compose-Linux-x86_64" -O /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo /usr/local/bin/docker-compose version
sudo apt-add-repository ppa:ansible/ansible -y
sudo apt-get update
sudo apt-get install ansible cowsay -y
