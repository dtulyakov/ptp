# Инструкция по запуску
Должны быть установлены [Vagrant](https://www.vagrantup.com/downloads.html) и [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Для запуска `ptp` контейнеров надо выполнить
```bash
cd time && ./run.sh
```
Для запуска мониторинга надо выполнить
```bash
cd zabbix && ./run.sh
```
**И импортировать из каталога XML все темплейты скрины и набор хостов**
Заббикс доступен по адресу [10.0.0.200](http://10.0.0.200)
* Логин/пароль стандартные - `Admin/zabbix`
* Порядок иморта указан номерами, последний `99` импортировать через 5 - 10 минут, что бы discovery успел получить данные (иначе будет ошибка)

В итоге выглядит так
[![Zabbix screen](https://gitlab.com/dtulyakov/ptp/raw/master/src/png/screen.png)](https://gitlab.com/dtulyakov/ptp/blob/master/src/png/screen.png)
